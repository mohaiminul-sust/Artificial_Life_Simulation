/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package als;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import org.LiveGraph.LiveGraph;
import org.LiveGraph.dataFile.write.DataStreamWriter;
import org.LiveGraph.settings.DataFileSettings;

/**
 *
 * @author Shibbir
 *
 * This class will generate Life. properties contains properties of life
 */
public class ThreadCon extends obstacle_and_dna_claculation {

    String dir_name;
    LameLand c = new LameLand();
    Random rand1;
    int total_thread_count = 0, total_generation_count = 0, total_food_eaten = 0, total_poison_eaten = 0,total_male=0,total_female=0;
    LogGenerator log_main = new LogGenerator();
    LogGenerator log_parent = new LogGenerator();
    boolean check_program_run = true;
    DataStreamWriter out = null;
    GraphBuilder graph = new GraphBuilder();
    long startTime;
    //Thread t = new Thread(new threadGen());

    /**
     * threadGen class is Life. properties_of_life is a class for properties of
     * life in LameLand like move,eat
     */
    
    public ThreadCon(){
        for(int i=0;i<dna_str.length;++i){
            dna_str[i]="";
        }
    }
    public class threadGen extends properties_of_life implements Runnable {

       // LogGenerator log_thread = new LogGenerator();
        String thread_name,gendar;
        int generation_no;
        String[] dna_sequence=new String[DNA_len];
        Thread_information thread_information;
        // public  int n=0,if_child=0,birth_period=20;
        long start_time_for_thread=System.nanoTime();
        public threadGen(int gen) {
            generation_no = gen;
            move_toad = 100;//100;
            rand = new Random();
            x = rand.nextInt(100);
            y = rand.nextInt(100);
            k = x + y * 100;
            if (k == 10000) {
                k--;
            }
        }

        public threadGen(int b_k, int gen,String[] dna_seq) {
            generation_no = gen;
            if (generation_no > total_generation_count) {
                total_generation_count = generation_no;
            }
            dna_sequence=dna_seq;
            move_toad = 49;
            k = b_k;
            x = k % 100;
            y = k / 100;
            rand = new Random();

            if (k == 10000) {
                k--;
            }
        }

        @Override

        public void run() {

            thread_name = Thread.currentThread().getName();
           // log_thread.LG(dir_name + "/" + thread_name+".txt");
           
            String[] st = thread_name.split("-");
            thread_no = Integer.parseInt(st[1]);
            thread_information=new Thread_information();
            thread_info[thread_no]=thread_information;
            gendar=thread_information.gendar;
            
            if(generation_no==1){
               dna_sequence=gen_random_dan_sequence(dna,thread_no,DNA_len);
            }
            else
            {
                push_dna_sequence(dna,dna_sequence,thread_no);
            }
            String dna_string="";
            // String dna_string=dna_sequence[0]+dna_sequence[1]+dna_sequence[2]+dna_sequence[3]+dna_sequence[4]+dna_sequence[5]+dna_sequence[6]+dna_sequence[7]+dna_sequence[8]+dna_sequence[9];
             for(int i=0;i<dna_sequence.length;++i){
                dna_string=dna_string+dna_sequence[i];
            }
             dna_str[thread_no]=dna_string;
             String s = Thread.currentThread().getName();
             System.out.println("Birth of " + s+" DNA "+dna_string);
          //  log_thread.write("dna seq: "+dna_string);
         //   log_thread.write("Gendar: "+thread_information.gendar);
            if(thread_information.gendar.equals("male")){
                
                total_male++;
            }
            else
                total_female++;
            
         //   log_thread.write("move, food_type, probability");
            try {
                lol();

            } catch (InterruptedException ex) {
                Logger.getLogger(ThreadCon.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        public void lol() throws InterruptedException {

            birth_gap = 0;
            
            int birth_gap_range;
            
            if(gendar.equals("male"))
            c.label[k].setBorder(javax.swing.BorderFactory.createLineBorder(Color.blue, 100));
            else
            c.label[k].setBorder(javax.swing.BorderFactory.createLineBorder(Color.red, 100));
            
            String s = Thread.currentThread().getName();
            thread_information.set_initial_position(k);
            
           //  log_thread.write("move, food_type, probability");
           thread_information.set_data(move_toad, generation_no, thread_no, dna_str[thread_no], k);
            int total_food_count = 0, total_poison_count = 0, total_children_count = 0;
            for (int i = 0; i < move_toad; ++i) {
//               System.out.println("food_posi+"+food_position[0]);
                int thread_no_of_call_thread=0;
                String food_type = eat(k,c, return_fodd_position(), return_obs_position());
                if(food_type.equals("food")) {
                    total_food_count++;
                    total_food_eaten++;
                    
                    energy+=30;
                } else if (food_type.equals("poison")) {
                    total_poison_count++;
                    total_poison_eaten++;
                }
                //System.out.println(i);
                //c.label[k].setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 255)));
                int k_before_move = k;
                set_life_data(thread_no, k, move_toad, birth_gap, position_of_lives, birth_gap_of_life, total_move_toad);
                int chek = move1(food_position, obstacle_position,position_of_lives);

                birth_gap = return_birth_gap(thread_no, birth_gap_of_life);
                birth_gap_range=thread_information.birth_gap;
                if(energy>0)
                if (birth_gap > birth_gap_range) {
                    int check_collition = Collition_between_two_thread2(thread_no, position_of_lives, birth_gap_of_life, total_move_toad,thread_info);
                    if (check_collition != 0) // System.out.println(check_collition);
                    {
                        if (check_collition > 1) {
                            //System.out.println("th return "+check_collition);
                            thread_no_of_call_thread=check_collition;
                            String[] dna_sequence_for_child=gen_dna_sequence(dna, thread_no, thread_no_of_call_thread,DNA_len);
                            give_brth(k, ++generation_no,dna_sequence_for_child);
            //                log_thread.write("Child Spawned!!!");
                            total_children_count++;
                            Thread.sleep(2000);
                            birth_gap = 0;
                            // System.out.println("Baccha hoiche!!!");
                            chek = move1(food_position, obstacle_position,position_of_lives);
                        }
                    }
                }
                birth_gap = return_birth_gap(thread_no, birth_gap_of_life);
                set_life_data(thread_no, k, move_toad, birth_gap, position_of_lives, birth_gap_of_life, total_move_toad);

                c.label[k_before_move].setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 255)));
               // int color_blue =50+move_toad;
                if(gendar.equals("male"))
            c.label[k].setBorder(javax.swing.BorderFactory.createLineBorder(Color.blue, 100));
            else
            c.label[k].setBorder(javax.swing.BorderFactory.createLineBorder(Color.red, 100));
            
           //     log_thread.write(Integer.toString(k) + ", " + food_type + ", " + chek);
                Thread.sleep(600);
                ++birth_gap;

                set_life_data(thread_no, k, move_toad, birth_gap, position_of_lives, birth_gap_of_life, total_move_toad);
                if(energy>0)
                energy--;
                //thread_info[thread_no].energy_for_reproduction=energy;
                thread_information.energy_for_reproduction=energy;
                if(move_toad>250)
                    break;
            }
            String dna_string="";
 //String dna_string=dna_sequence[0]+dna_sequence[1]+dna_sequence[2]+dna_sequence[3]+dna_sequence[4]+dna_sequence[5]+dna_sequence[6]+dna_sequence[7]+dna_sequence[8]+dna_sequence[9];
            for(int i=0;i<dna_sequence.length;++i){
                dna_string=dna_string+dna_sequence[i];
            }
            
            long end_time_for_thread=System.nanoTime();
            long alive_time=end_time_for_thread-start_time_for_thread;
            log_parent.write(thread_name + ", " + move_toad + ", " +alive_time+"," + total_food_count + ", " + total_poison_count + ", " + total_children_count+", "+dna_string+","+gendar+","+generation_no+","+start_time_for_thread+","+end_time_for_thread);
        //    log_thread.closefile();
            c.label[k].setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 255)));
            position_of_lives[thread_no] = -10;
          // writer.write( Thread.currentThread().getName()+" "+total_move[thread_no]);
            // c.label[k].setIcon(null);  

        }

    }

    public void graphMaker() {

        out = graph.GraphBuilderInit(out, dir_name + "/graph", "Graph data for ALS");
        settings(dir_name + "/graph", 1000);
        startTime = System.currentTimeMillis();
        LiveGraph app = LiveGraph.application();
        app.exec(new String[]{"-dfs", "session.lgdfs"});

        graph.setColumn(out, new String[]{"Time", "Population", "Food", "Poison"});
    }

    private static void settings(String f_name, int freq) {
        DataFileSettings dfs = new DataFileSettings();
        dfs.setDataFile(f_name + ".lgdat");
        dfs.setUpdateFrequency(freq);
        dfs.save("session.lgdfs");
    }

    private void give_brth(int k, int gen, String[] dna_sequence_for_child) {
        Thread t_new = new Thread(new threadGen(k, gen,dna_sequence_for_child));
        t_new.start();
        total_thread_count++;
    }

    public class status_gui_update implements Runnable {

        @Override

        public void run() {
            StatusGUI statusgui = new StatusGUI();
            statusgui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            statusgui.setVisible(true);
            graphMaker();
            int alive = statusgui.update(total_thread_count, position_of_lives, total_generation_count, total_food_eaten, total_poison_eaten);
            while (check_program_run) {
                try {
                    Thread.sleep(1000);
                    graph.writeData(out, new Double[]{(double) System.currentTimeMillis() - startTime, (double) alive, (double) total_food_eaten, (double) total_poison_eaten});
                    alive = statusgui.update(total_thread_count, position_of_lives, total_generation_count, total_food_eaten, total_poison_eaten);
                    if(alive==0){
                        Thread.sleep(10000);
                        check_program_run=false;
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(ThreadCon.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

        }

    }
    
        public class set_program_data implements Runnable {

        @Override

        public void run() {
            int time_count=0,copied_dna=0,unique_dna=0;
            ArrayList list = new ArrayList();
            String info=new String("");
            int number_of_run=return_runtime();
           // System.out.println("slfksjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");
            while (check_program_run) {
                try {
                    
                    Thread.sleep(60000*3);
                    time_count+=3;
                    copied_dna=check_dna_match();
                    unique_dna=total_thread_count-copied_dna;
                    //info="number_of_run,"+time_count+","+copied_dna+","+unique_dna+","+total_thread_count+","+total_generation_count;
                    info=return_program_data(time_count,number_of_run);
                   // System.out.println("jjjjjjjjjjjjjjjjjjjjjjj   "+info);
                    list.add(info);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ThreadCon.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            info=return_program_data(time_count,number_of_run);
            list.add(info);
            LogGenerator log_all=new LogGenerator();
          //  log_all.add_text(list);
        }

    }
        
         public class set_program_view implements Runnable {

        @Override

        public void run() {
            LogGenerator view=new LogGenerator();
            view.LG(dir_name+"/View.csv");
            view.write("view_number,position,situation");
            int number_of_shot=0;
           while (check_program_run) {
               
               number_of_shot++;
                try {
                    Thread.sleep(6000);
                    int[] position_situation=new int[10000];
                    
                    for(int i=0;i<position_of_lives.length;++i){
                        if(position_of_lives[i]>0){
                            position_situation[position_of_lives[i]]=1;
                        }
                    }
                    
                    for(int i=0;i<food_position.length;++i){
                        if(food_position[i]>0){
                            position_situation[food_position[i]]=2;
                            
                        }
                    }
                    /*
                    for(int i=0;i<10000;++i){
                        if(position_situation[i]!=1 || position_situation[i]!=2){
                            position_situation[i]=0;
                        }
                    }*/
                    for(int i=0;i<position_situation.length;++i){
                        view.write(number_of_shot+","+i+","+position_situation[i]);
                    }
                    
                    
                   
                } catch (InterruptedException ex) {
                    Logger.getLogger(ThreadCon.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            
           view.closefile();
         
        }

    }
        
        private String return_program_data(int time,int number_of_run){
          int  copied_dna=check_dna_match();
             int       unique_dna=total_thread_count-copied_dna;
             
          String string =number_of_run+","+(time)+","+copied_dna+","+unique_dna+","+total_thread_count+","+total_male+","+total_female+","+total_generation_count;
            
            return string;
        }
        
        private int return_runtime(){
          
       	
        String fileName="D:/ALS_data.csv";
        BufferedReader reader = null;
        ArrayList list = new ArrayList();
        int number_of_run=0;
    try {
        reader = new BufferedReader(new FileReader(fileName));
        String tmp;
        while ((tmp = reader.readLine()) != null)
        list.add(tmp);
        //OUtil.closeReader(reader);
        reader.close();
        
        String string=list.get((list.size()-1)).toString();
        //System.out.println(string+"jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");
        String[] arra=string.split(",");
       // System.out.println(arra[0]+"jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");
        number_of_run=Integer.parseInt(arra[0]);
} catch (IOException e) {
        System.out.println("Check");
} 
            return number_of_run+1;
        }
        
    
    
     public class food_generator implements Runnable {

        @Override

        public void run() {
            gen_food(c);
            clear_food(food_position,c);
            while (check_program_run) {
                try {
                    
                   regenerate_food(c);
                   Thread.sleep(5000);
                    
                    clear_food(food_position,c);
                    //System.out.println("dj");
                } catch (InterruptedException ex) {
                    Logger.getLogger(ThreadCon.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

        }

    }
     private void als_Update(long time,int copy_dnaA,int total_threadA,int total_maleA,int total_femaleA,int gen_count){
         int number_of_run=return_runtime();
         LogGenerator lg=new LogGenerator();
         int unique_DNA=total_threadA-copy_dnaA;
         String s=number_of_run+","+time+","+copy_dnaA+","+total_threadA+","+unique_DNA+","+total_maleA+","+total_femaleA+","+gen_count+","+DNA_len;
         lg.add_text(s);
         
     }

    public void threadEngine() {
 //log_parent.add_text("alu");
        /**
         * initialising GUI
         */
        
       long start_time=System.nanoTime();
        dir_name = "Log " + LocalDateTime.now().toString();
        dir_name = dir_name.replace(":", "-");
        System.out.println(dir_name);
        Accessories ac = new Accessories();
        if (ac.createDir(dir_name)) {
            System.out.println("Folder Created : " + dir_name);
        }
        log_parent.LG(dir_name + "/ThreadInfo.csv");
        log_parent.write("thread name, total move,alive_time,total_food_count,total_poison_count,total_children_count,dna,gender,genaration_no,start_time,end_time");
        c.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        c.setSize(750, 600);
        c.setVisible(true);

        
        Thread food_gen=new Thread(new food_generator());
       
        food_gen.start();
       // Thread set_data=new Thread(new set_program_data());
       // set_data.start();
        
        //gen_food(c);    //generate food
       // gen_obstacle(c);

        /**
         * Life starts from here
         */
      Thread create_view=new Thread(new set_program_view());
      create_view.start();
        for (int i = 0; i < 80; ++i) {
            Thread t7 = new Thread(new threadGen(1));
            t7.start();

            total_thread_count = i + 1;
        }
        total_generation_count = 1;
        status_gui_update status = new status_gui_update();
        status.run();
       // System.out.println("mmmmmmmmmmmmmmmmmmmmmmmmmnnnnn");
        
//        SHUTDOWN HOOK
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            
            System.out.println("Closing parent Thread...............................");
            graph.closeGraph(out);
            log_parent.closefile();
            log_main.LG(dir_name + "/logMain.txt");
            log_main.write("total thread no, total generation count, total food eaten, total poison eaten,total male thread,total femal thread");
            log_main.write(total_thread_count + "," + total_generation_count + "," + total_food_eaten + "," + total_poison_eaten+","+ total_male+","+total_female);
            log_main.closefile();
            int copied_dna_number=check_dna_match();
            System.out.println("DNA MATCH: "+copied_dna_number);
            write_dna_match(dir_name);
            check_program_run = false;
            generation_log(dir_name,"/ThreadInfo.csv",total_generation_count);
            long end_time=System.nanoTime();
            long runtime=end_time-start_time;
            als_Update(runtime,copied_dna_number,total_thread_count, total_male, total_female, total_generation_count);
            System.out.println("total food "+total_food_eaten+" ");
            
        }));

         
         
    }

}
