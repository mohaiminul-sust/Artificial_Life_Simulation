/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package als;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Shibbir
 * 
 */
/**
 * 
 * properties contains properties of life. Food for Life
 */
public class properties {
    int x_axis,y_axis,max_agent_num,int_agent_num,tot_food_num,gen1_int_move_,child_int_move,
            inc_move_on_food,female_birth_gap,male_birth_gap,DNA_len=90,percent_empty,percent_food,
            percent_food_if_hungry,percent_attraction,percent_attraction_if_horney;
    int number_of_life=10000;
    public void generation_log(String dir,String thread_info_filename,int total_generation){
        
        int[] child=new int[total_generation];
        int[] child_male=new int[total_generation];
        int[] child_female=new int[total_generation];
        long[] start_time_of_gen=new long[total_generation];
        long[] end_time_of_gen=new long[total_generation];
        
        for(int i=0;i<child.length;++i){
            child[i]=0;child_female[i]=0;child_male[i]=0;start_time_of_gen[i]=0;end_time_of_gen[i]=0;
        }
       
        BufferedReader reader = null;
        //BufferedWriter write = null;
        ArrayList list = new ArrayList();
    try {
        reader = new BufferedReader(new FileReader(dir+thread_info_filename));
        String tmp;
        while ((tmp = reader.readLine()) != null)
        list.add(tmp);
        //OUtil.closeReader(reader);
        reader.close();
        
} catch (IOException e) {
        System.out.println("Check");
} 
    
    String[] thread_information=new String[11];
     for(int i=1;i<list.size();i++){
        thread_information=list.get(i).toString().split(",");

        int gen_number=Integer.parseInt(thread_information[8])-1;
        child[gen_number]++;
        if(thread_information[7].equals("male"))
            child_male[gen_number]++;
        else
           child_female[gen_number]++;
        
        long lowest_start_time=-1;
        long highest_end_time=0;
        
        if(start_time_of_gen[gen_number]==0 || (start_time_of_gen[gen_number]<Long.parseLong(thread_information[9])) ){
start_time_of_gen[gen_number]=Long.parseLong(thread_information[9]);           

        }
        
        if(end_time_of_gen[gen_number]==0 || (end_time_of_gen[gen_number]<Long.parseLong(thread_information[10])) ){
            end_time_of_gen[gen_number]=Long.parseLong(thread_information[10]);
        }
        
    }
    LogGenerator generation_log_create=new LogGenerator();
    generation_log_create.LG(dir+"/Generation_log.csv");
    generation_log_create.write("generation_number,offsprings_per_generation,male_offsprings_per_generation,female_offsprings_per_generation,duration");
    for(int i=0;i<child.length;i++){
        generation_log_create.write((i+1)+","+child[i]+","+child_male[i]+","+child_female[i]+","+(end_time_of_gen[i]-start_time_of_gen[i]));
    }
    generation_log_create.closefile();
    }

}
