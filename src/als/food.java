/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package als;

import java.awt.Color;
import java.util.Random;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author Shibbir
 */

public class food extends properties{
    
    int food_number=600;
      int[] food_position= new int[food_number];  // This array contains position of food
      int[] food_position_constant=new int[food_number];
      public int[] position_of_lives=new int[number_of_life];   //position of life in LameLand... Thread-2's position will be written like that position_of_lives[2]=k of Thread-2
        public int[] birth_gap_of_life=new int[number_of_life];
        public int[] total_move_toad=new int[number_of_life];
        public int[] energy_of_lives=new int[number_of_life];
        public Thread_information[] thread_info=new Thread_information[number_of_life];
    
      /**
       * 
       * gen_food method generate food and set coordinate  in food_position 
 colour of food is red
       
 e 4 region in thisForm 1st regLameLande y=0 to y=24, 2nd y=25 to y=49, 3rd y=50 to y=74, 4th y=75 to y=99 for well distributed food 
     * @param c
       */
    public void gen_food (LameLand c){
         for(int i=0; i<food_position.length;++i){
            food_position[i]=0;
        }
        Random rand=new Random();
        int flag=1;
        int l=0;
        int random=0;
        for(l=0;l<food_position.length;++l){
            if(l<100){
              
                while(flag==1){
                    random=rand.nextInt(2500);
                    flag=check_empty_position(food_position, random);
                    
                }
                flag=1;
               
            }
            else if(l>=100 && l<200){
                
                                while(flag==1){
                    random=rand.nextInt(2500)+2500;
                    flag=check_empty_position(food_position, random);
                     
                }
                flag=1;
            }
            else if(l>=200 && l<300){
               
                    while(flag==1){
                    random=rand.nextInt(2500)+5000;
                    flag=check_empty_position(food_position, random);
                     
                }
                   flag=1;
            }
            else if(l>=300 && l<400){
               
                    while(flag==1){
                    random=rand.nextInt(2500)+7500;
                    flag=check_empty_position(food_position, random);
                     
                }
                 flag=1;
            }
            food_position[l]=random;
           // System.out.println("Food Position"+l+"="+food_position[l]);
             c.label[food_position[l]].setBorder(javax.swing.BorderFactory.createLineBorder(Color.green, 100));
            
        }
        for(int i=0;i<food_position.length;++i)
        food_position_constant[i]=food_position[i];
        
    }
    public void regenerate_food(LameLand c){
        for(int i=0;i<food_position.length;++i){
            food_position[i]=food_position_constant[i];
             c.label[food_position[i]].setBorder(javax.swing.BorderFactory.createLineBorder(Color.green, 100));
        }
        
    }
    
    public int make_tree(int l,int[] positions,int position){
        positions[l++]=position-101;
        positions[l++]=position-100;
        positions[l++]=position-99;
        positions[l++]=position-1;
        positions[l++]=position;
        positions[l++]=position+1;
        positions[l++]=position+99;
        positions[l++]=position+100;
        positions[l++]=position+101;
        return l;
    }
    
    public int check_empty_position(int array[],int check_int){
        //System.out.println("zdnbkb "+check_int);
        for(int i=0;i<array.length;++i){
            if(array[i]==check_int)
                return 1;
        }
        return 0;
        
    }
    public int[] return_fodd_position(){
        return food_position;
    }
    
    public void clear_food(int[] food_position,LameLand c){
          for(int i=0; i<food_position.length;++i){
              if(food_position[i]!=-1){
                  c.label[food_position[i]].setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 255)));
                food_position[i]=-1;
              }
        }
    }
      
}
