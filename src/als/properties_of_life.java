/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package als;

/**
 *
 * @author Shibbir
 */
public class properties_of_life extends Life_Move{
    
        public String eat(int k,LameLand c,int[] food_position1,int[] obstacle_position){
            String s=Thread.currentThread().getName();
            
            int check_position=check_empty_position(food_position1, k);
            
            if(check_position==1){
                if(move_toad<high_level_move_toad){
                    move_toad+=20;//10;
                    clear_position(k, food_position1);
                    return "food";
                   // System.out.println(s+"    MoveTaoad="+move_toad);
                }
            }
            else if(check_position==0){
                check_position=check_empty_position(obstacle_position, k);
                if(check_position==1){
                    move_toad-=5;
                    clear_position(k, obstacle_position);
                    return "poison";
                  //  System.out.println(s+"            MoveTaoad="+move_toad);
                }
            }
                    
            return "nothing";

    }
        private void clear_position(int position,int[] array){
            for(int i=0;i<array.length;++i){
                if(array[i]==position)
                    array[i]=-1;
            }
        }
        
       public String[] gen_dna_sequence(String[][] dna,int thread_no_a,int thread_no_b,int DNA_len){
          int start_b=rand.nextInt(DNA_len),stop_b=rand.nextInt(DNA_len);
          
          int temp=start_b;
          String[] sequence=new String[DNA_len];
          if(start_b>stop_b)
          {
              start_b=stop_b;
              stop_b=temp;
          }
          for(int i=0;i<sequence.length;i++){
              
              if(i>=start_b && i<=stop_b){
                  sequence[i]=dna[thread_no_b][i];
              }
              else
                  sequence[i]=dna[thread_no_a][i];
              
              
          }
           System.out.println("Coll by "+thread_no_a+" and "+thread_no_b +" "+start_b+"  "+stop_b);
           
        return sequence;
    }
       
       
       public void push_dna_sequence(String[][] dna,String[] sequence,int thread_no){
           for(int i=0;i<sequence.length;++i){
               dna[thread_no][i]=sequence[i];
           }
       }
       
       
       public String[] gen_random_dan_sequence(String[][] dna,int thread_no,int DNA_len){
           String[] sequence=new String[DNA_len];
           int random_seq=0;//0=A ,1=T,2=G,3=C
           for(int i=0;i<sequence.length;i++){
               random_seq=rand.nextInt(4);
               if(random_seq==0)
               sequence[i]="a";
               else if(random_seq==1)
                   sequence[i]="t";
               else if(random_seq==2)
                   sequence[i]="c";
               else if(random_seq==3)
                   sequence[i]="g";

               dna[thread_no][i]=sequence[i];
              /*
               if(random_seq==0)
               sequence[i]="A";
               else if(random_seq==1)
                   sequence[i]="T";
               else if(random_seq==2)
                   sequence[i]="G";
               else if(random_seq==3)
                   sequence[i]="C";
               dna[thread_no][i]=sequence[i];
*/           
}
           
           return sequence;
       }
}
