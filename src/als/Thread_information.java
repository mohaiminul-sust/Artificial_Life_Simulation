/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package als;

import java.util.Random;

/**
 *
 * @author OSMAN
 */
public class Thread_information {
    //life_move count number of move it can exsist, birth_gap for count gap to produce thread
    public int life_move_number,birth_gap,generation,thread_no,initial_position,last_position,energy_for_reproduction,present_life_move_number; 
    public String dna,gendar;//dna means dna sequence,gendar indicate male or female thread
    public Thread_information(){
        life_move_number=0;
        birth_gap=0;
        generation=0;
        thread_no=0;
        initial_position=-1;
        last_position=-1;
        energy_for_reproduction=0;
        present_life_move_number=0;
        dna="";
        gendar="";
        Random rand=new Random();
        int find_gendar=rand.nextInt(2);
        if(find_gendar==1)
            gendar="male";
        else
            gendar="female";
        
        if(gendar.equals("male"))
            birth_gap=2;
        else
            birth_gap=20;
    }
    
    public void set_initial_position(int int_position){
        initial_position=int_position;
    }
    
    public void set_data(int int_move_toad,int gen,int thrd_no,String dna_seq,int last_k){
        life_move_number=int_move_toad;
        generation=gen;
        thread_no=thrd_no;
        dna=dna_seq;
       last_position=last_k;
    }
    public void set_last_position(int last_k,int energy,int move_number){
        last_position=last_k;
        energy_for_reproduction=energy;
        present_life_move_number=move_number;
    }
}
